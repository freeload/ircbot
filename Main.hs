import Network ( withSocketsDo, connectTo, PortID(PortNumber) )
import System.IO
    ( hGetContents,
      hSetBuffering,
      BufferMode(NoBuffering),
      hPutStr,
      stdout )
import IRCReader ( parseStream )
import System.Random ( getStdGen, randomRs )
import Control.Monad.State.Lazy ( evalState )

main = withSocketsDo $ do
    h <- connectTo "irc.freenode.org" (PortNumber 6667)
    hSetBuffering h NoBuffering
    hSetBuffering stdout NoBuffering
    hPutStr h "NICK john\r\n"
    hPutStr h "USER john * * *\r\n"
    c <- hGetContents h
    mapM_ onMsg $ parseStream c
    return ()

ircBot h = runState $ do
    :q


onMsg (("",(nick,_,_)),"PRIVMSG",[channel, msg]) =
    return ()
--putStrLn $ concat [channel, ": ", nick, ": ", msg]
onMsg (_, "443", _) = return ()

onMsg ((hostname, ("",_,_)),cmd,args) = print (cmd, args)
