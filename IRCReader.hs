module IRCReader (
    parseStream,
    parseMsg,
    parsePrefix,
    parseParams
) where
import Text.Regex.TDFA ((=~))

parseStream s = [parseMsg x | [_,x] <- (s =~ "([^\r]*)\r\n") :: [[String]]]

msgRegex = "^(:([^ ]*) *)?([^ ]*)(.*)$"
parseMsg m = let [[_,_,x,y,z]] = m =~ msgRegex :: [[String]]
    in (parsePrefix x, y, parseParams z)

prefixRegex = "^([A-Za-z]([A-Za-z0-9{}`\\[\\\\\\^]|\\]|\\-)+)(!([^@]*))?(@(.*))?$|^(.*)$"
parsePrefix p = let [[_,a,_,_,b,_,c,d]] = p =~ prefixRegex :: [[String]] in (d,(a,b,c))

paramsRegex = "( +([^: ][^ ]*))| +:(.*)" 
parseParams p = [if trail=="" then normal else trail | [_,_,normal,trail] <- p =~ paramsRegex :: [[String]]]
